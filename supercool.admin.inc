<?php

/**
 * Implements hook_form().
 *
 * @see supercool_admin_form_validate()
 * @see supercool_admin_form_submit()
 */
function supercool_admin_form($form, &$form_state)
{
    $form['tabs'] = array(
        '#type' => 'vertical_tabs',
        '#tree' => FALSE,
    );

    /*
     * Core tweaks
     */

    $form['tabs']['core'] = array(
        '#type' => 'fieldset',
        '#title' => 'Core',
    );

    $form['tabs']['core']['supercool_core_hide_frontpage_title'] = array(
        '#type' => 'checkbox',
        '#title' => t('Hide title on frontpage'),
        '#default_value' => variable_get('supercool_core_hide_frontpage_title'),
    );

    $form['tabs']['core']['supercool_core_trim_item_path'] = array(
        '#type' => 'checkbox',
        '#title' => t('Trim menu item path'),
        '#default_value' => variable_get('supercool_core_trim_item_path'),
    );

    $form['tabs']['core']['supercool_core_fix_language_negotiation'] = array(
        '#type' => 'checkbox',
        '#title' => t('Fix browser language detection if page cache is enabled'),
        '#default_value' => variable_get('supercool_core_fix_language_negotiation'),
        '#description' => t('Save <a href="@path">this form</a> again after enabling this tweak', array('@path' => '/admin/config/regional/language/configure')),
        '#access' => module_exists('locale'),
    );

    $form['tabs']['core']['supercool_core_add_error_class'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add CSS class on body of error pages'),
        '#default_value' => variable_get('supercool_core_add_error_class'),
    );

    $form['tabs']['core']['supercool_core_403_class'] = array(
        '#type' => 'textfield',
        '#title' => t('403 (access denied) class'),
        '#default_value' => variable_get('supercool_core_403_class', 'page-access-denied'),
        '#states' => array(
            'invisible' => array(
                'input[name="supercool_core_add_error_class"]' => array('checked' => FALSE),
            ),
        ),
    );

    $form['tabs']['core']['supercool_core_404_class'] = array(
        '#type' => 'textfield',
        '#title' => t('404 (not found) class'),
        '#default_value' => variable_get('supercool_core_404_class', 'page-not-found'),
        '#states' => array(
            'invisible' => array(
                'input[name="supercool_core_add_error_class"]' => array('checked' => FALSE),
            ),
        ),
    );

    $form['tabs']['core']['supercool_core_remove_breadcrumb'] = array(
        '#type' => 'checkbox',
        '#title' => t('Remove breadcrumb everywhere'),
        '#default_value' => variable_get('supercool_core_remove_breadcrumb'),
    );

    $form['tabs']['core']['supercool_core_trim_username_at_login'] = array(
        '#type' => 'checkbox',
        '#title' => t('Trim username at login'),
        '#default_value' => variable_get('supercool_core_trim_username_at_login'),
    );

    $form['tabs']['core']['supercool_core_onetimelogin_custom'] = array(
        '#type' => 'checkbox',
        '#title' => t('Customize the one-time login links expiration'),
        '#default_value' => variable_get('supercool_core_onetimelogin_custom'),
        '#access' => (version_compare(VERSION, '7.15') >= 0),
    );

    $form['tabs']['core']['supercool_core_onetimelogin_timeout'] = array(
        '#type' => 'textfield',
        '#title' => t('Timeout (in seconds)'),
        '#default_value' => variable_get('user_password_reset_timeout'),
        '#states' => array(
            'invisible' => array(
                'input[name="supercool_core_onetimelogin_custom"]' => array('checked' => FALSE),
            ),
        ),
        '#description' => t('Default value is %value seconds', array('%value' => variable_get('supercool_core_onetimelogin_default_timeout', variable_get('user_password_reset_timeout', 86400)))),
        '#access' => (version_compare(VERSION, '7.15') >= 0),
    );

    $form['tabs']['core']['supercool_core_strip_tags_head_title'] = array(
        '#type' => 'checkbox',
        '#title' => t('Strip HTML tags from page &lt;title&gt;'),
        '#default_value' => variable_get('supercool_core_strip_tags_head_title'),
    );

    $form['tabs']['core']['template_suggestions'] = array(
        '#type' => 'fieldset',
        '#title' => t('Enable extra template suggestions'),
    );

    $form['tabs']['core']['template_suggestions']['supercool_core_template_suggestions'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Enable extra template suggestions'),
        '#title_display' => 'invisible',
        '#default_value' => variable_get('supercool_core_template_suggestions', array()),
        '#options' => array(
            'node__front' => '<em>node__front</em>',
            'node__type__viewmode' => '<em>node__[nodetype]__[viewmode]</em>',
            'page__403' => '<em>page__403</em>',
            'page__404' => '<em>page__404</em>',
            'page__type_nodetype' => '<em>page__type_[nodetype]</em>',
            'page__type_nodetype__nid' => '<em>page__type_[nodetype]__[nid]</em>',
        ),
    );

    $form['tabs']['core']['extra_hooks'] = array(
        '#type' => 'fieldset',
        '#title' => t('Enable extra hooks'),
    );

    $form['tabs']['core']['extra_hooks']['supercool_core_extra_hooks'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Enable extra hooks'),
        '#title_display' => 'invisible',
        '#default_value' => variable_get('supercool_core_extra_hooks', array()),
        '#options' => array(
            'user_blocked' => '<em>hook_supercool_user_blocked</em> : Called when a user is blocked',
            'user_unblocked' => '<em>hook_supercool_user_unblocked</em> : Called when a user is unblocked',
        ),
    );

    /*
     * Comment tweaks
     */

    if (module_exists('comment')) {

        $form['tabs']['comment'] = array(
            '#type' => 'fieldset',
            '#title' => 'Comment',
        );

        $form['tabs']['comment']['supercool_comment_hide_teaser_link'] = array(
            '#type' => 'checkbox',
            '#title' => 'Hide <em>Add new comment</em> link in <em>teaser</em> view mode',
            '#default_value' => variable_get('supercool_comment_hide_teaser_link'),
        );

        $form['tabs']['comment']['supercool_comment_hide_reply_comment_link'] = array(
            '#type' => 'checkbox',
            '#title' => 'Hide <em>Reply</em> link in comments',
            '#default_value' => variable_get('supercool_comment_hide_reply_comment_link'),
        );

        $form['tabs']['comment']['supercool_comment_hide_permalink'] = array(
            '#type' => 'checkbox',
            '#title' => 'Hide <em>Permalink</em> link in comments',
            '#default_value' => variable_get('supercool_comment_hide_permalink'),
        );

        $form['tabs']['comment']['supercool_comment_hide_permalink_on_title'] = array(
            '#type' => 'checkbox',
            '#title' => 'Remove link on comment title',
            '#default_value' => variable_get('supercool_comment_hide_permalink_on_title'),
        );
    }

    /*
     * Language Switcher Dropdown tweaks
     */

    if (module_exists('lang_dropdown')) {

        $form['tabs']['lang_dropdown'] = array(
            '#type' => 'fieldset',
            '#title' => 'Language Switcher Dropdown',
        );

        $form['tabs']['lang_dropdown']['supercool_lang_dropdown_remove_inline_style'] = array(
            '#type' => 'checkbox',
            '#title' => 'Remove unnecessary inline style on lang_dropdown_form',
            '#default_value' => variable_get('supercool_lang_dropdown_remove_inline_style'),
        );
    }

    /*
     * Link tweaks
     */

    if (module_exists('link')) {

        $form['tabs']['link'] = array(
            '#type' => 'fieldset',
            '#title' => 'Link',
        );

        /**
         * Link module < 7.x-1.5 was using 'link_extra_domains' variable
         * Link module >= 7.x-1.5 is using 'link_allowed_domains' variable
         */
        $link_variable = 'link_extra_domains';
        $info = system_get_info('module', 'link');
        if (!empty($info['version']) && version_compare($info['version'], '7.x-1.5') >= 0) {
            $link_variable = 'link_allowed_domains';
        }

        $form['tabs']['link']['supercool_link_extra_domains'] = array(
            '#type' => 'textfield',
            '#title' => t('Extra domains for URL validation'),
            '#default_value' => implode('|', variable_get($link_variable, array())),
            '#description' => 'Don\'t include domain dots. Separate extra domains by |',
            '#access' => (version_compare($info['version'], '7.x-1.1') >= 0),
        );
    }

    /*
     * Pathauto tweaks
     */

    if (module_exists('pathauto')) {

        $form['tabs']['pathauto'] = array(
            '#type' => 'fieldset',
            '#title' => 'Pathauto',
        );

        $form['tabs']['pathauto']['supercool_pathauto_add_extra_punctuation_chars'] = array(
            '#type' => 'checkbox',
            '#title' => t('Add commercial characters (@chars) for punctuation control', array('@chars' => '©, ®, ™')),
            '#default_value' => variable_get('supercool_pathauto_add_extra_punctuation_chars'),
            '#description' => 'These characters are used <a href="/admin/config/search/path/settings#edit-punctuation">here</a>',
        );

        $form['tabs']['pathauto']['supercool_pathauto_remove_alias_accents'] = array(
            '#type' => 'checkbox',
            '#title' => t('Remove accents from url alias'),
            '#default_value' => variable_get('supercool_pathauto_remove_alias_accents'),
        );

        $form['tabs']['pathauto']['supercool_pathauto_replace_ampersand'] = array(
            '#type' => 'checkbox',
            '#title' => t('Replace %search by the word %replace (translated)', array('%search' => '&', '%replace' => 'and')),
            '#default_value' => variable_get('supercool_pathauto_replace_ampersand'),
        );
    }

    /*
     * Search tweaks
     */

    if (module_exists('search')) {

        $form['tabs']['search'] = array(
            '#type' => 'fieldset',
            '#title' => 'Search',
        );

        $form['tabs']['search']['supercool_search_remove_info_from_search_results'] = array(
            '#type' => 'checkbox',
            '#title' => t('Remove user name and modification date from search results'),
            '#default_value' => variable_get('supercool_search_remove_info_from_search_results'),
        );

        $form['tabs']['search']['supercool_search_hide_form_on_results_page'] = array(
            '#type' => 'checkbox',
            '#title' => t('Hide search form on search results page'),
            '#default_value' => variable_get('supercool_search_hide_form_on_results_page'),
        );
    }

    /*
     * Taxonomy tweaks
     */

    if (module_exists('taxonomy')) {

        $form['tabs']['taxonomy'] = array(
            '#type' => 'fieldset',
            '#title' => 'Taxonomy',
        );

        $form['tabs']['taxonomy']['supercool_taxonomy_fix_empty_title'] = array(
            '#type' => 'checkbox',
            '#title' => t('Fix empty <em>name</em> bug if it has been replaced with a field instance by <em>Title</em> module'),
            '#default_value' => variable_get('supercool_taxonomy_fix_empty_title'),
            '#access' => module_exists('title'),
        );

        $form['tabs']['taxonomy']['supercool_taxonomy_hide_rss_icon'] = array(
            '#type' => 'checkbox',
            '#title' => t('Hide RSS feed icon on taxonomy pages'),
            '#default_value' => variable_get('supercool_taxonomy_hide_rss_icon'),
        );

        $form['tabs']['taxonomy']['supercool_taxonomy_hide_rss_icon_everywhere'] = array(
            '#type' => 'checkbox',
            '#title' => t('Hide RSS feed icon on all pages'),
            '#default_value' => variable_get('supercool_taxonomy_hide_rss_icon_everywhere'),
        );
    }

    /*
     * Views tweaks
     */

    if (module_exists('views')) {

        $form['tabs']['views'] = array(
            '#type' => 'fieldset',
            '#title' => 'Views',
        );

        $form['tabs']['views']['supercool_views_enable_body_class'] = array(
            '#type' => 'checkbox',
            '#title' => t('Add CSS class(es) on body of Views displayed as pages'),
            '#default_value' => variable_get('supercool_views_enable_body_class'),
        );

        $form['tabs']['views']['supercool_views_body_class'] = array(
            '#type' => 'textfield',
            '#title' => t('CSS class(es)'),
            '#default_value' => variable_get('supercool_views_body_class', 'views-type-page views-[name]'),
            '#description' => t('Separate multiple classes by space. <ul><li>[name] will be replaced by view name</li><li>[display] will be replaced by view display</li></ul>'),
            '#states' => array(
                'invisible' => array(
                    'input[name="supercool_views_enable_body_class"]' => array('checked' => FALSE),
                ),
            ),
        );
    }

    /*
     * Site Map tweaks
     */

    if (module_exists('site_map')) {

        $form['tabs']['site_map'] = array(
            '#type' => 'fieldset',
            '#title' => 'Site Map',
        );

        $form['tabs']['site_map']['supercool_site_map_main_menu_first'] = array(
            '#type' => 'checkbox',
            '#title' => t('Display <em>main-menu</em> first'),
            '#default_value' => variable_get('supercool_site_map_main_menu_first'),
        );

        $form['tabs']['site_map']['supercool_site_map_hide_excluded_by_xmlsitemap'] = array(
            '#type' => 'checkbox',
            '#title' => t('Hide menu items excluded by <em>XML sitemap menu</em> module'),
            '#default_value' => variable_get('supercool_site_map_hide_excluded_by_xmlsitemap'),
            '#access' => module_exists('xmlsitemap_menu'),
        );
    }

    /*
     * Percentage tweaks
     */

    if (module_exists('percentage')) {

        $form['tabs']['percentage'] = array(
            '#type' => 'fieldset',
            '#title' => 'Percentage',
        );

        $form['tabs']['percentage']['supercool_percentage_add_field_suffix'] = array(
            '#type' => 'checkbox',
            '#title' => t('Add <em>%</em> as field suffix'),
            '#default_value' => variable_get('supercool_percentage_add_field_suffix'),
        );
    }

    /*
     * Date tweaks
     */

    if (module_exists('date')) {

        $form['tabs']['date'] = array(
            '#type' => 'fieldset',
            '#title' => 'Date',
        );

        $form['tabs']['date']['supercool_date_hide_popup_description'] = array(
            '#type' => 'checkbox',
            '#title' => t('Hide element description on Popup-calendar widgets'),
            '#default_value' => variable_get('supercool_date_hide_popup_description'),
        );

        $form['tabs']['date']['supercool_date_force_date_label'] = array(
            '#type' => 'checkbox',
            '#title' => t('Force refresh date labels'),
            '#description' => 'Check this option if date label is always "Date" instead of label set in field settings',
            '#default_value' => variable_get('supercool_date_force_date_label'),
        );
    }

    /*
     * Domain tweaks
     */

    if (module_exists('domain') && module_exists('domain_settings')) {

        $form['tabs']['domain'] = array(
            '#type' => 'fieldset',
            '#title' => 'Domain',
        );

        $form['tabs']['domain']['supercool_domain_use_active_on_site_information_settings'] = array(
            '#type' => 'checkbox',
            '#title' => t('Use active domain as default domain on system settings form'),
            '#description' => 'Prevent to save the site information settings on "All domains" (default choice)',
            '#default_value' => variable_get('supercool_domain_use_active_on_site_information_settings'),
        );

        $form['tabs']['domain']['hooks'] = array(
            '#type' => 'fieldset',
            '#title' => t('Extra hooks'),
        );

        $form['tabs']['domain']['hooks']['supercool_domain_extra_hooks'] = array(
            '#type' => 'checkboxes',
            '#title' => t('Extra hooks'),
            '#title_display' => 'invisible',
            '#default_value' => variable_get('supercool_domain_extra_hooks', array()),
            '#options' => array(
                'domain_settings_ignore' => '<em>hook_supercool_domain_settings_ignore</em> : Called to hide the "Domain Specific Settings" form',
            ),
        );
    }

    /*
     * Domain Blocks tweaks
     */
    if (module_exists('domain') && module_exists('domain_blocks')) {

        $form['tabs']['domain_blocks'] = array(
            '#type' => 'fieldset',
            '#title' => 'Domain blocks',
        );

        $form['tabs']['domain_blocks']['supercool_domain_blocks_context_compatibility'] = array(
            '#type' => 'checkbox',
            '#title' => t('Force the visibility check in hook_block_view_alter()'),
            '#description' => 'Check this option to make domain_blocks compatible with Context module',
            '#default_value' => variable_get('supercool_domain_blocks_context_compatibility'),
        );
    }

    /*
     * Contact tweaks
     */

    if (module_exists('contact')) {

        $form['tabs']['contact'] = array(
            '#type' => 'fieldset',
            '#title' => 'Contact Form',
        );

        $form['tabs']['contact']['supercool_contact_send_copy'] = array(
            '#type' => 'radios',
            '#title' => t('"Send me a copy" behaviour for anonymous users'),
            '#default_value' => variable_get('supercool_contact_send_copy', ''),
            '#description' => t('Warning : allow anonymous users to send themselves a copy may be used to spam people.'),
            '#options' => array(
                '' => t('Default : Do not allow anonymous users to send themselves a copy'),
                'checkbox' => t('Checkbox : Let anonymous users choose to send themselves a copy'),
                'copy' => t('Copy : Always send a copy to anonymous users'),
            ),
        );
        $form['tabs']['contact']['supercool_contact_populate_subject'] = array(
            '#type' => 'checkbox',
            '#title' => t('Allow the subject field to be populated by the <em>subject</em> request parameter'),
            '#description' => 'Use /contact?subject=hello',
            '#default_value' => variable_get('supercool_contact_populate_subject'),
        );
        $form['tabs']['contact']['supercool_contact_populate_message'] = array(
            '#type' => 'checkbox',
            '#title' => t('Allow the message field to be populated by the <em>message</em> request parameter'),
            '#description' => 'Use /contact?message=hello',
            '#default_value' => variable_get('supercool_contact_populate_message'),
        );
    }

    /*
     * Linked Field tweaks
     */

    if (module_exists('linked_field')) {

        $form['tabs']['linked_field'] = array(
            '#type' => 'fieldset',
            '#title' => 'Linked Field',
        );

        $form['tabs']['linked_field']['supercool_linked_field_translate_title'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable translatable link title'),
            '#default_value' => variable_get('supercool_linked_field_translate_title'),
        );
    }

    /*
     * Masquerade tweaks
     */

    if (module_exists('masquerade')) {

        $form['tabs']['masquerade'] = array(
            '#type' => 'fieldset',
            '#title' => 'Masquerade',
        );

        $form['tabs']['masquerade']['supercool_masquerade_message_alter_hook'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable hook_supercool_maquerade_message_alter'),
            '#default_value' => variable_get('supercool_masquerade_message_alter_hook'),
        );
    }

    /*
     * i18n menu
     */

    if (module_exists('i18n_menu')) {

        $form['tabs']['i18n_menu'] = array(
            '#type' => 'fieldset',
            '#title' => 'Menu translation',
        );

        $form['tabs']['i18n_menu']['i18n_menu_language_selector_weight'] = array(
            '#type' => 'checkbox',
            '#title' => t('Force the language selector to appear first on the menu_edit_item form'),
            '#default_value' => variable_get('i18n_menu_language_selector_weight'),
        );
    }

    /*
     * Bean tweaks
     */

    if (module_exists('bean')) {

        $form['tabs']['bean'] = array(
            '#type' => 'fieldset',
            '#title' => 'Bean',
        );

        $form['tabs']['bean']['bean_disable_revisions'] = array(
            '#type' => 'checkbox',
            '#title' => t('Disable revisions on all beans'),
            '#default_value' => variable_get('bean_disable_revisions'),
        );
    }

    /*
     * Webform tweaks
     */

    if (module_exists('webform')) {

        $form['tabs']['webform'] = array(
            '#type' => 'fieldset',
            '#title' => 'Webform',
        );

        $form['tabs']['webform']['supercool_webform_add_after_checkbox'] = array(
            '#type' => 'checkbox',
            '#title' => t('Add "After" option for label of checkboxes'),
            '#default_value' => variable_get('supercool_webform_add_after_checkbox'),
        );
    }

    $form['#submit'][] = 'supercool_admin_form_submit';

    return system_settings_form($form);
}

/**
 * Form validation handler for supercool_admin_form().
 */
function supercool_admin_form_validate($form, &$form_state)
{
    // the key of hidden fields (#access == false) is not in $form_state['input']
    if (isset($form_state['input']['supercool_core_onetimelogin_timeout'])) {
        $form_state['values']['supercool_core_onetimelogin_timeout'] = (int)$form_state['values']['supercool_core_onetimelogin_timeout'];
    }
}

/**
 * Form submission handler for supercool_admin_form().
 */
function supercool_admin_form_submit($form, &$form_state)
{
    if (isset($form_state['input']['supercool_link_extra_domains'])) {

        $domains = explode('|', $form_state['values']['supercool_link_extra_domains']);

        /**
         * Link module < 7.x-1.5 was using 'link_extra_domains' variable
         * Link module >= 7.x-1.5 is using 'link_allowed_domains' variable
         */
        $link_variable = 'link_extra_domains';
        $info = system_get_info('module', 'link');
        if (!empty($info['version']) && version_compare($info['version'], '7.x-1.5') >= 0) {
            $link_variable = 'link_allowed_domains';
        }

        variable_set($link_variable, $domains);
        // don't need this value as a variable
        unset($form_state['values']['supercool_link_extra_domains']);
    }

    // the key of hidden fields (#access == false) is not in $form_state['input']
    if (isset($form_state['input']['supercool_core_onetimelogin_timeout'])) {

        if ($form_state['values']['supercool_core_onetimelogin_custom']) {

            // keep a backup of user_password_reset_timeout variable
            if (!variable_get('supercool_core_onetimelogin_default_timeout')) {
                variable_set('supercool_core_onetimelogin_default_timeout', variable_get('user_password_reset_timeout'));
            }

            variable_set('user_password_reset_timeout', $form_state['values']['supercool_core_onetimelogin_timeout']);

        } else {

            if (variable_get('supercool_core_onetimelogin_default_timeout')) {
                variable_set('user_password_reset_timeout', variable_get('supercool_core_onetimelogin_default_timeout'));
                variable_del('supercool_core_onetimelogin_default_timeout');
            }
        }

        // don't need this value as a variable
        unset($form_state['values']['supercool_core_onetimelogin_timeout']);
    }

    if (isset($form_state['input']['supercool_pathauto_replace_ampersand'])) {
        if (!empty($form_state['values']['supercool_pathauto_replace_ampersand'])) {
            variable_set('supercool_pathauto_punctuation_ampersand_backup', variable_get('pathauto_punctuation_ampersand'));
            variable_set('pathauto_punctuation_ampersand', 2); /** @see Pathauto constant PATHAUTO_PUNCTUATION_DO_NOTHING */
        } else {
            if (variable_get('supercool_pathauto_punctuation_ampersand_backup', null) !== null) {
                variable_set('pathauto_punctuation_ampersand', variable_get('supercool_pathauto_punctuation_ampersand_backup'));
                variable_del('supercool_pathauto_punctuation_ampersand_backup');
            }
        }
    }
}
